(function () {
    $('.date-11').datepicker({
        format: "dd/mm/yyyy",
        endDate: "today",
        language: "es",
        autoclose: true
    });
    $('.enfermoTrue').hide();
    $('.aereoTrue').hide();
})();

const setEnfermo = () => {
    var e = document.getElementById('enfermo30dias');
    var strEnf = e.options[e.selectedIndex].value;
    if (strEnf == 'true') {
        $('#enfermedadexterior').attr("required", true);
        $('#fechaprimerossintomas').attr("required", true);
        $('.enfermoTrue').show();
    } else {
        $('#enfermedadexterior').attr("required", false);
        $('#fechaprimerossintomas').attr("required", false);
        $('.enfermoTrue').hide();
    }
};

(function () {
    var input = document.querySelector("#phone1")
    // initialise plugin
    var iti = intlTelInput(input, {
        initialCountry: 'cl',
        utilsScript: "./intl-tel-input/js/utils.js"
    });
    // on blur: validate
    input.addEventListener('change', function () {
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                input.setCustomValidity("")
            } else {
                input.setCustomValidity("Numero Invalido")
            }
        }
    });
})();
(function () {
    var input = document.querySelector("#phone2")
    // initialise plugin
    var iti = intlTelInput(input, {
        initialCountry: 'cl',
        utilsScript: "./intl-tel-input/js/utils.js"
    });
    // on blur: validate
    input.addEventListener('change', function () {
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                input.setCustomValidity("")
            } else {
                input.setCustomValidity("Numero Invalido")
            }
        }
    });
})();
(function () {
    var input = document.querySelector("#phone3")
    // initialise plugin
    var iti = intlTelInput(input, {
        initialCountry: 'cl',
        utilsScript: "./intl-tel-input/js/utils.js"
    });
    // on blur: validate
    input.addEventListener('change', function () {
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                input.setCustomValidity("")
            } else {
                input.setCustomValidity("Numero Invalido")
            }
        }
    });
})();


var integrantes = 0;
var maxInegrantes = 5;

const setFamiliares = () => {
    $("#agregarIntegrante").on("click", () => {
        if (integrantes === maxInegrantes) {
            $("#agregarIntegrante").text("La cantidad máxima de integrantes es 5");
        } else {
            integrantes++;

            $("#integrantes").append(`
          <label>PERSONA ${integrantes}</label>

          <div class="col-md-12 mb-3">
            <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase();"
              id="apellidopaterno${integrantes}" name="apellidopaterno${integrantes}" placeholder="APELLIDO PATERNO *" pattern="^[a-zA-Z ]*$" title="No puede ingresar números o signos"  required />           
          </div>
          <div class="col-md-12 mb-3">
            <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase();"
              id="apellidomaterno${integrantes}" name="apellidomaterno${integrantes}" placeholder="APELLIDO MATERNO" pattern="^[a-zA-Z ]*$" title="No puede ingresar números o signos"  />
          </div>
          <div class="col-md-12 mb-3">
            <input type="texto" class="form-control" id="nombres${integrantes}" name="nombres${integrantes}" placeholder="NOMBRES *" pattern="^[a-zA-Z ]*$" title="No puede ingresar números o signos" required />
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="sexo${integrantes}" required>
              <option value="">SEXO *</option>
              <option value="FEMENINO">FEMENINO</option>
              <option value="MASCULINO">MASCULINO</option>
            </select>
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="nacionalidad${integrantes}" required>
              <option value="">SELECCIONE SU NACIONALIDAD *</option>
              ${_PAISES_
                    .map(
                        option =>
                            `<option value=${option.id_pais}>${option.nombre}</option>`
                    )
                    .join("")}
            </select>
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="consanguinidad${integrantes}" required>
              <option value="">SELECCIONE PARENTESCO *</option>
              ${_PARENTESCO_
                    .map(
                        option =>
                            `<option value=${option.value}>${option.nombre}</option>`
                    )
                    .join("")}
            </select>
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="documentodeviaje${integrantes}" id="documentodeviaje${integrantes}" required>
              <option value="">SELECCIONE SU DOCUMENTO DE VIAJE</option>
              <option value="rut">RUT</option>
              <option value="run">RUN</option>
              <option value="pasaporte">Pasaporte</option>
              <option value="dni">DNI</option>
            </select>
          </div>
          <div class="col-md-12 mb-3">
            <input type="texto" class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="numerodocumentodeviaje${integrantes}" id="numerodocumentodeviaje${integrantes}"
              placeholder="NÚMERO DOCUMENTO DE VIAJE *" required />
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="contactopersonaenferma${integrantes}" id="contactopersonaenferma${integrantes}" required>
              <option value="">TUVO CONTACTO CON UNA PERSONA ENFERMA DE CORONAVIRUS (COVID-19)? *</option>
              <option value="true">SI</option>
              <option value="false">NO</option>
            </select>
          </div>
            `);
            setRutValidator(integrantes);
        }
    });
};

var direcciones = 1;
var maxDirecciones = 5;

const setDirecciones = () => {
    $("#agregarDireccion").on("click", () => {
        if (direcciones === maxDirecciones) {
            $("#agregarDireccion").text("La cantidad máxima de direcciones es 5");
        } else {
            direcciones++;

            $("#direcciones").append(`
      <!-- VIA RESIDENCIA ${direcciones}-->
      <div class="col-md-12 mb-3">
        <h6><b>DIRECCIÓN ${direcciones}</b></h6>
        <label for="via">RESIDENCIA / HOTEL / HOSPEDAJE</label><span class="rojo">*</span>
        <select class="form-control" name="via${direcciones}" required>
          <option value="">SELECCIONE</option>
          <option value="1">RESIDENCIA</option>
          <option value="2">HOTEL</option>
          <option value="3">HOSPEDAJE</option>
        </select>       
      </div>
      <div id="nombre_hotel_input" class="col-md-12 mb-3">
        <label for="depto">NOMBRE DEL LUGAR DONDE SE HOSPEDÓ</label>(SI CORRESPONDE)
        <input type="text" class="form-control" name="hotel${direcciones}" placeholder="HOTEL" />        
      </div>
      <div class="col-md-12 mb-3">
        <label for="direccion${direcciones}">DIRECCIÓN</label><span class="rojo">*</span>
        <input type="text" class="form-control" name="direccion${direcciones}" placeholder="DIRECCIÓN" pattern="^[a-zA-Z0-9 ]*$" required />       
      </div>
      <div class="col-md-12 mb-3">
        <label for="numero">NÚMERO</label><span class="rojo">*</span>
        <input type="text" class="form-control" name="numero${direcciones}" placeholder="NÚMERO" pattern="^[a-zA-Z0-9 ]*$" required />
      </div>
      <div class="col-md-12 mb-3">
        <label for="depto">DEPARTAMENTO</label>
        <input type="text" class="form-control" name="depto${direcciones}" placeholder="DEPARTAMENTO" pattern="^[a-zA-Z0-9 ]*$" />       
      </div>
      <div class="col-md-12 mb-3">
        <label for="region">REGIÓN</label><span class="rojo">*</span>
        <select class="form-control" id="region${direcciones}" name="region${direcciones}" required>
          <option value="">SELECCIONE UNA REGIÓN</option>
        </select>       
      </div>
      <div class="col-md-12 mb-3">
        <label for="comuna">COMUNA</label><span class="rojo">*</span>
        <select class="form-control" id="comuna${direcciones}" name="comuna${direcciones}" required>
          <option value="">SELECCIONE UNA COMUNA</option>
        </select>       
      </div>
      <!-- direc ${direcciones}-->`);

            handleOptionAdding("region" + direcciones, _REGION_, "id_region");
            subscribeOnChangeRegion(direcciones);
        }
    });
};
const _PARENTESCO_ = [
    {
        value: 1,
        nombre: "Esposo/a"
    },
    {
        value: 2,
        nombre: "Hijo/a"
    },
    {
        value: 3,
        nombre: "Hermano/a"
    },
    {
        value: 4,
        nombre: "Padre"
    },
    {
        value: 5,
        nombre: "Madre"
    },
    {
        value: 6,
        nombre: "Abuelo/a"
    },
    {
        value: 7,
        nombre: "Tio/a"
    },
    {
        value: 8,
        nombre: "Primo/a"
    },
    {
        value: 9,
        nombre: "Otro/a"
    }
];


const _MEDIOINGRESO_ = [
    {
        id_medio: 1,
        nombre: "AÉREO"
    },
    {
        id_medio: 2,
        nombre: "MARÍTIMO"
    },
    {
        id_medio: 3,
        nombre: "TERRESTRE"
    }
];

const _MEDIOS_ = [
    {
        id_medio: 1,
        nombre: "Avion"
    },
    {
        id_medio: 2,
        nombre: "Barco"
    },
    {
        id_medio: 3,
        nombre: "Bus"
    },
    {
        id_medio: 4,
        nombre: "Auto"
    },
    {
        id_medio: 5,
        nombre: "Tren"
    },
    {
        id_medio: 6,
        nombre: "Camion"
    },
    {
        id_medio: 7,
        nombre: "Moto"
    },
    {
        id_medio: 8,
        nombre: "Otro"
    }
];