const handleOptionAdding = function (selectName, json, idName) {
  const select = document.getElementById(selectName);

  json.forEach(function (country) {
    handleIteration(country, select, idName);
  });
};

const handleIteration = function (obj, select, idName) {
  const newOption = document.createElement("option");

  newOption.appendChild(document.createTextNode(obj.nombre));
  newOption.value = obj[idName];
  newOption.text = obj.nombre;

  select.appendChild(newOption);
};

const subscribeOnChangeRegion = (id) => {
  document.getElementById("region" + id).onchange = function (event) {
    var selectedRegionId = event.target.value;
    var comuna = document.getElementById("comuna" + id);
    while (comuna.childElementCount > 1) {
      comuna.removeChild(comuna.lastElementChild);
    }
    if (!selectedRegionId) {
      return;
    }
    var filteredComunas = _COMUNAS_.filter(function (comunaJson) {
      return comunaJson.id_region === Number(selectedRegionId);
    });
    filteredComunas.forEach(function (comunaJson) {
      var newOption = document.createElement("option");
      newOption.appendChild(document.createTextNode(comunaJson.nombre));
      newOption.value = comunaJson["id_comuna"];
      newOption.text = comunaJson.nombre;
      comuna.appendChild(newOption);
    });
  };
};
const _DANGERCOUNTRIES_ = [46];
// const _DANGERCOUNTRIES_ = [73, 112, 4, 82, 47, 107, 58, 114, 46];

const visitedselectConstructor = () => {
  handleOptionAdding(
    "visitedselect",
    _PAISES_.filter((n) => {
      return !_DANGERCOUNTRIES_.includes(n.id_pais);
    }),
    "id_pais"
  );
  document.getElementById("visitedselect").onchange = function (event) {
    const newinput = document.createElement("input");
    const value = event.target.value;
    if (!value) {
      document.getElementById("visitedselect").value = "";
      return;
    }
    const toremove = event.target.options[event.target.selectedIndex];
    document.getElementById("visitedselect").removeChild(toremove);
    const selectedCountry = _PAISES_.filter(function (country) {
      return country.id_pais === Number(value);
    })[0];
    newinput.value = selectedCountry.id_pais;
    newinput.name = "paises_visitados";
    newinput.type = "checkbox";
    newinput.checked = true;
    document.getElementById("addpaises").append(newinput);
    document.getElementById("addpaises").append(" " + selectedCountry.nombre);
    document.getElementById("addpaises").append(document.createElement("br"));
    document.getElementById("visitedselect").value = "";
  };
};

const toogleSintomasRequired = () => {
  for (sintomas of document.getElementsByName("sintomas")) {
    sintomas.required = true;
    sintomas.onchange = (e) => {
      if (e.target.checked) {
        for (sintomas of document.getElementsByName("sintomas")) {
          sintomas.required = false;
        }
        for (sintomas of document.getElementsByName("sintomasNone")) {
          sintomas.required = false;
          sintomas.checked = false;
        }
      } else {
        var atleastone = false;
        for (sintomas of document.getElementsByName("sintomas")) {
          if (sintomas.checked) {
            atleastone = true;
            break;
          }
        }
        if (atleastone) {
          for (sintomas of document.getElementsByName("sintomas")) {
            sintomas.required = false;
          }
          for (sintomas of document.getElementsByName("sintomasNone")) {
            sintomas.required = false;
          }
        } else {
          for (sintomas of document.getElementsByName("sintomas")) {
            sintomas.required = true;
          }
          for (sintomas of document.getElementsByName("sintomasNone")) {
            sintomas.required = true;
          }
        }
      }
    };
  }
  for (sintomasNone of document.getElementsByName("sintomasNone")) {
    sintomasNone.required = true;
    sintomasNone.onchange = (e) => {
      if (e.target.checked) {
        for (sintomas of document.getElementsByName("sintomas")) {
          sintomas.required = false;
          sintomas.checked = false;
        }
      } else {
        e.target.required = true;
        for (sintomas of document.getElementsByName("sintomas")) {
          sintomas.required = true;
        }
      }
    };
  }
};

function clean(rut) {
  return typeof rut === "string"
    ? rut.replace(/^0+|[^0-9kK]+/g, "").toUpperCase()
    : "";
}

function validateRUT(rut) {
  function clean(rut) {
    return typeof rut === "string"
      ? rut.replace(/^0+|[^0-9kK]+/g, "").toUpperCase()
      : "";
  }

  if (typeof rut !== "string") {
    return false;
  }
  if (!/^0*(\d{1,3}(\.?\d{3})*)-?([\dkK])$/.test(rut)) {
    return false;
  }

  rut = clean(rut);

  var t = parseInt(rut.slice(0, -1), 10);
  var m = 0;
  var s = 1;

  while (t > 0) {
    s = (s + (t % 10) * (9 - (m++ % 6))) % 11;
    t = Math.floor(t / 10);
  }

  var v = s > 0 ? "" + (s - 1) : "K";
  return v === rut.slice(-1);
}

const isValidNumber = function (type, number) {
  switch (type) {
    case "conductor_rut":
    case "empresa_rut":
      const pattern = /^[0-9]{7,8}-[0-9K]{1}$/;
      if (validateRUT(number) && pattern.test(number)) {
        return true;
      } else {
        return false;
      }
    case "pasaporte":
      const pattern2 = /[A-Z0-9a-z]/;
      return pattern2.test(number);
    default:
      return false;
  }
};

const setRutValidator = () => {
  var rutEmpresa = document.getElementById("empresa_rut");
  var rutConductor = document.getElementById("conductor_rut");
  rutEmpresa.onchange = (e) => {
    const doc = "empresa_rut";
    if (!doc || !isValidNumber(doc, e.target.value)) {
      rutEmpresa.setCustomValidity("Documento Inválido");
    } else {
      rutEmpresa.setCustomValidity("");
    }
  };
  rutConductor.onchange = (e) => {
    const doc = "conductor_rut";
    if (!doc || !isValidNumber(doc, e.target.value)) {
      rutConductor.setCustomValidity("Documento Inválido");
    } else {
      rutConductor.setCustomValidity("");
    }
  };
};

const setDate = () => {
  if (
    document.getElementById("fecha_salida") &&
    document.getElementById("hora_salida")
  ) {
    var fecha = document.getElementById("fecha_salida").value;
    var hora = document.getElementById("hora_salida").value;
    console.log("fecha", fecha);
    console.log("hora", hora);
    if (fecha != "" && hora != "") {
      document.getElementById("fechahora_salida").value = fecha + " " + hora;
    }
  }
};

(function () {
  var input = document.querySelector("#conductor_celular");
  // initialise plugin
  var iti = intlTelInput(input, {
    initialCountry: "cl",
    utilsScript: "./intl-tel-input/js/utils.js",
  });
  // on blur: validate
  input.addEventListener("change", function () {
    if (input.value.trim()) {
      if (iti.isValidNumber()) {
        input.setCustomValidity("");
      } else {
        input.setCustomValidity("Numero Invalido");
      }
    }
  });
  $(".date-11").datepicker({
    format: "dd/mm/yyyy",
    endDate: "today",
    language: "es",
    autoclose: true,
  });
  $(".date-12").datepicker({
    format: "dd/mm/yyyy",
    language: "es",
    autoclose: true,
  });
  // a la confirmacion de mail obligandolo que sean iguales
  document.getElementById("email").onchange = (e) => {
    var confirmemail = document.getElementById("confirmemail");
    confirmemail.pattern = e.target.value;
  };
  // dropdown regiones
  if (document.getElementById("region1"))
    handleOptionAdding("region1", _REGION_, "id_region");

  subscribeOnChangeRegion(1);

  if (document.getElementById("region2"))
    handleOptionAdding("region2", _REGION_, "id_region");

  subscribeOnChangeRegion(2);

  document.getElementById("empresa_rut").onchange = (e) => {
    const pattern = /^[0-9]{7,8}-[0-9K]{1}$/;
    if (!e || !pattern.test(e.target.value)) {
      document
        .getElementById("empresa_rut")
        .setCustomValidity("Documento Inválido");
    } else {
      document.getElementById("empresa_rut").setCustomValidity("");
    }
  };

  document.getElementById("conductor_rut").onchange = (e) => {
    const pattern = /^[0-9]{7,8}-[0-9K]{1}$/;
    if (!e || !pattern.test(e.target.value)) {
      document
        .getElementById("conductor_rut")
        .setCustomValidity("Documento Inválido");
    } else {
      document.getElementById("conductor_rut").setCustomValidity("");
    }
  };
  setRutValidator();
})();
