(function () {
    $('.date-11').datepicker({
        format: "dd/mm/yyyy",
        endDate: "today",
        autoclose: true
    });
    $('.enfermoTrue').hide();
    $('.aereoTrue').hide();
})();

const setEnfermo = () => {
    var e = document.getElementById('enfermo30dias');
    var strEnf = e.options[e.selectedIndex].value;
    if (strEnf == 'true') {
        $('#enfermedadexterior').attr("required", true);
        $('#fechaprimerossintomas').attr("required", true);
        $('.enfermoTrue').show();
    } else {
        $('#enfermedadexterior').attr("required", false);
        $('#fechaprimerossintomas').attr("required", false);
        $('.enfermoTrue').hide();
    }
};

(function () {
    var input = document.querySelector("#phone1")
    // initialise plugin
    var iti = intlTelInput(input, {
        initialCountry: 'cl',
        utilsScript: "./intl-tel-input/js/utils.js"
    });
    // on blur: validate
    input.addEventListener('change', function () {
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                input.setCustomValidity("")
            } else {
                input.setCustomValidity("Numero Invalido")
            }
        }
    });
})();
(function () {
    var input = document.querySelector("#phone2")
    // initialise plugin
    var iti = intlTelInput(input, {
        initialCountry: 'cl',
        utilsScript: "./intl-tel-input/js/utils.js"
    });
    // on blur: validate
    input.addEventListener('change', function () {
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                input.setCustomValidity("")
            } else {
                input.setCustomValidity("Numero Invalido")
            }
        }
    });
})();
(function () {
    var input = document.querySelector("#phone3")
    // initialise plugin
    var iti = intlTelInput(input, {
        initialCountry: 'cl',
        utilsScript: "./intl-tel-input/js/utils.js"
    });
    // on blur: validate
    input.addEventListener('change', function () {
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                input.setCustomValidity("")
            } else {
                input.setCustomValidity("Numero Invalido")
            }
        }
    });
})();



var integrantes = 0;
var maxInegrantes = 5;

const setFamiliares = () => {
    $("#agregarIntegrante").on("click", () => {
        if (integrantes === maxInegrantes) {
            $("#agregarIntegrante").text("Max number of members is 5");
        } else {
            integrantes++;

            $("#integrantes").append(`
          <label>PERSONA ${integrantes}</label>

          <div class="col-md-12 mb-3">
            <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase();"
              id="apellidopaterno${integrantes}" name="apellidopaterno${integrantes}" placeholder="LASTNAME *" pattern="^[a-zA-Z ]*$" required />           
          </div>
          <div class="col-md-12 mb-3">
            <input type="text" class="form-control" onkeyup="this.value = this.value.toUpperCase();"
              id="apellidomaterno${integrantes}" name="apellidomaterno${integrantes}" placeholder="MOTHERS LASTNAME" pattern="^[a-zA-Z ]*$" />
          </div>
          <div class="col-md-12 mb-3">
            <input type="texto" class="form-control" id="nombres${integrantes}" name="nombres${integrantes}" placeholder="NAMES *" pattern="^[a-zA-Z ]*$" required />
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="sexo${integrantes}" required>
              <option value="">SEX *</option>
              <option value="FEMENINO">FEMALE</option>
              <option value="MASCULINO">MALE</option>
            </select>
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="nacionalidad${integrantes}" required>
              <option value="">NACIONALITY *</option>
              ${_PAISES_
                    .map(
                        option =>
                            `<option value=${option.id_pais}>${option.nombre}</option>`
                    )
                    .join("")}
            </select>
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="consanguinidad${integrantes}" required>
              <option value="">CHOOSE RELATIONSHIP *</option>
              ${_PARENTESCO_
                    .map(
                        option =>
                            `<option value=${option.value}>${option.nombre}</option>`
                    )
                    .join("")}
            </select>
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="documentodeviaje${integrantes}" id="documentodeviaje${integrantes}" required>
              <option value="">CHOOSE TRAVEL DOCUMENT *</option>
                <option value="rut">RUT</option>
                <option value="run">RUN</option>
                <option value="pasaporte">Passport</option>
                <option value="dni">DNI</option>
            </select>
          </div>
          <div class="col-md-12 mb-3">
            <input type="texto" class="form-control" onkeyup="this.value = this.value.toUpperCase();" name="numerodocumentodeviaje${integrantes}" id="numerodocumentodeviaje${integrantes}"
              placeholder="TRAVEL DOCUMENT NUMBER *" required />
          </div>
          <div class="col-md-12 mb-3">
            <select class="form-control" name="contactopersonaenferma${integrantes}" id="contactopersonaenferma${integrantes}" required>
              <option value="">WERE IN CONTACT WITH ANY PERSON ILL WITH CORONAVIRUS (COVID-19)? *</option>
              <option value="true">YES</option>
              <option value="false">NO</option>
            </select>
          </div>
            `);
            setRutValidator(integrantes);
        }
    });
};

var direcciones = 1;
var maxDirecciones = 5;

const setDirecciones = () => {
    $("#agregarDireccion").on("click", () => {
        if (direcciones === maxDirecciones) {
            $("#agregarDireccion").text("La cantidad máxima de direcciones es 5");
        } else {
            direcciones++;

            $("#direcciones").append(`
      <!-- VIA RESIDENCIA ${direcciones}-->
      <div class="col-md-12 mb-3">
        <h6><b>DIRECCIÓN ${direcciones}</b></h6>
        <label for="via">RESIDENCE / HOTEL / LODGING</label><span class="rojo">*</span>
        <select class="form-control" name="via${direcciones}" required>
          <option value="">CHOOSE</option>
          <option value="1">RESIDENCE</option>
          <option value="2">HOTEL</option>
          <option value="3">LODGING</option>
        </select>       
      </div>
      <div id="nombre_hotel_input" class="col-md-12 mb-3">
        <label for="depto">LODGING NAME</label>(IF APPLICABLE)
        <input type="text" class="form-control" name="hotel${direcciones}" placeholder="LODGING" />        
      </div>
      <div class="col-md-12 mb-3">
        <label for="direccion${direcciones}">ADDRESS</label><span class="rojo">*</span>
        <input type="text" class="form-control" name="direccion${direcciones}" placeholder="ADDRESS" pattern="^[a-zA-Z0-9 ]*$" required />       
      </div>
      <div class="col-md-12 mb-3">
        <label for="numero">NUMBER</label><span class="rojo">*</span>
        <input type="text" class="form-control" name="numero${direcciones}" placeholder="NUMBER" pattern="^[a-zA-Z0-9 ]*$" required />
      </div>
      <div class="col-md-12 mb-3">
        <label for="depto">APARTMENT</label>
        <input type="text" class="form-control" name="depto${direcciones}" placeholder="APARTMENT" pattern="^[a-zA-Z0-9 ]*$" />       
      </div>
      <div class="col-md-12 mb-3">
        <label for="region">REGION</label><span class="rojo">*</span>
        <select class="form-control" id="region${direcciones}" name="region${direcciones}" required>
          <option value="">CHOOSE REGION</option>
        </select>       
      </div>
      <div class="col-md-12 mb-3">
        <label for="comuna">COMMUNE</label><span class="rojo">*</span>
        <select class="form-control" id="comuna${direcciones}" name="comuna${direcciones}" required>
          <option value="">CHOOSE</option>
        </select>       
      </div>
      <!-- direc ${direcciones}-->`);

            handleOptionAdding("region" + direcciones, _REGION_, "id_region");
            subscribeOnChangeRegion(direcciones);
        }
    });
};
const _PARENTESCO_ = [
    {
        value: 1,
        nombre: "spouse"
    },
    {
        value: 2,
        nombre: "child"
    },
    {
        value: 3,
        nombre: "brother"
    },
    {
        value: 4,
        nombre: "father"
    },
    {
        value: 5,
        nombre: "mother"
    },
    {
        value: 6,
        nombre: "grandfather"
    },
    {
        value: 7,
        nombre: "uncle"
    },
    {
        value: 8,
        nombre: "cousin"
    },
    {
        value: 9,
        nombre: "Other"
    }
];

const _MEDIOINGRESO_ = [
    {
        id_medio: 1,
        nombre: "AIR"
    },
    {
        id_medio: 2,
        nombre: "SEA"
    },
    {
        id_medio: 3,
        nombre: "LAND"
    }
];

const _MEDIOS_ = [
    {
        id_medio: 1,
        nombre: "Plane"
    },
    {
        id_medio: 2,
        nombre: "Vessel"
    },
    {
        id_medio: 3,
        nombre: "Bus"
    },
    {
        id_medio: 4,
        nombre: "Car"
    },
    {
        id_medio: 5,
        nombre: "Train"
    },
    {
        id_medio: 6,
        nombre: "Truck"
    },
    {
        id_medio: 7,
        nombre: "Motorcycle"
    },
    {
        id_medio: 8,
        nombre: "Other"
    }
];